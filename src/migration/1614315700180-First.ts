import {MigrationInterface, QueryRunner} from "typeorm";

export class First1614315700180 implements MigrationInterface {
    name = 'First1614315700180'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "countries" ("id" SERIAL NOT NULL, "name" character varying NOT NULL, "country_code" character varying NOT NULL, CONSTRAINT "UQ_fa1376321185575cf2226b1491d" UNIQUE ("name"), CONSTRAINT "UQ_c73ac097c97c09610beeac26f5f" UNIQUE ("country_code"), CONSTRAINT "PK_b2d7006793e8697ab3ae2deff18" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "edates" ("id" SERIAL NOT NULL, "date" TIMESTAMP WITH TIME ZONE NOT NULL, CONSTRAINT "PK_1983b8f0469c4acd4b3514eaa3d" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "events" ("id" SERIAL NOT NULL, "name" character varying NOT NULL, "description" text NOT NULL, CONSTRAINT "UQ_dfa3d03bef3f90f650fd138fb38" UNIQUE ("name"), CONSTRAINT "PK_40731c7151fe4be3116e45ddf73" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "users" ("id" SERIAL NOT NULL, "username" character varying NOT NULL, "email" character varying NOT NULL, "spiritual_name" character varying, "createdAt" TIMESTAMP NOT NULL DEFAULT '"2021-02-26T05:01:43.385Z"', CONSTRAINT "UQ_fe0bb3f6520ee0469504521e710" UNIQUE ("username"), CONSTRAINT "UQ_97672ac88f789774dd47f7c8be3" UNIQUE ("email"), CONSTRAINT "PK_a3ffb1c0c8416b9fc6f907b7433" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "post" ("id" SERIAL NOT NULL, "title" character varying NOT NULL, "content" text NOT NULL, "created_at" TIMESTAMP NOT NULL DEFAULT now(), "updated_at" TIMESTAMP NOT NULL DEFAULT now(), "authorId" integer, CONSTRAINT "PK_be5fda3aac270b134ff9c21cdee" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "events_countries_countries" ("eventsId" integer NOT NULL, "countriesId" integer NOT NULL, CONSTRAINT "PK_22bf2238a9d289344f53dc6840a" PRIMARY KEY ("eventsId", "countriesId"))`);
        await queryRunner.query(`CREATE INDEX "IDX_2662d5ee0a8feea71112e8ce0d" ON "events_countries_countries" ("eventsId") `);
        await queryRunner.query(`CREATE INDEX "IDX_fdbedf506060a2c393c4e7daee" ON "events_countries_countries" ("countriesId") `);
        await queryRunner.query(`CREATE TABLE "events_edates_edates" ("eventsId" integer NOT NULL, "edatesId" integer NOT NULL, CONSTRAINT "PK_54a520431e2aa98b73d66003712" PRIMARY KEY ("eventsId", "edatesId"))`);
        await queryRunner.query(`CREATE INDEX "IDX_e27a3cfd22add5dbe01b11e01e" ON "events_edates_edates" ("eventsId") `);
        await queryRunner.query(`CREATE INDEX "IDX_28bbfc9cd49669ac86e5abee22" ON "events_edates_edates" ("edatesId") `);
        await queryRunner.query(`ALTER TABLE "post" ADD CONSTRAINT "FK_c6fb082a3114f35d0cc27c518e0" FOREIGN KEY ("authorId") REFERENCES "users"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "events_countries_countries" ADD CONSTRAINT "FK_2662d5ee0a8feea71112e8ce0d0" FOREIGN KEY ("eventsId") REFERENCES "events"("id") ON DELETE CASCADE ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "events_countries_countries" ADD CONSTRAINT "FK_fdbedf506060a2c393c4e7daee1" FOREIGN KEY ("countriesId") REFERENCES "countries"("id") ON DELETE CASCADE ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "events_edates_edates" ADD CONSTRAINT "FK_e27a3cfd22add5dbe01b11e01ea" FOREIGN KEY ("eventsId") REFERENCES "events"("id") ON DELETE CASCADE ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "events_edates_edates" ADD CONSTRAINT "FK_28bbfc9cd49669ac86e5abee226" FOREIGN KEY ("edatesId") REFERENCES "edates"("id") ON DELETE CASCADE ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "events_edates_edates" DROP CONSTRAINT "FK_28bbfc9cd49669ac86e5abee226"`);
        await queryRunner.query(`ALTER TABLE "events_edates_edates" DROP CONSTRAINT "FK_e27a3cfd22add5dbe01b11e01ea"`);
        await queryRunner.query(`ALTER TABLE "events_countries_countries" DROP CONSTRAINT "FK_fdbedf506060a2c393c4e7daee1"`);
        await queryRunner.query(`ALTER TABLE "events_countries_countries" DROP CONSTRAINT "FK_2662d5ee0a8feea71112e8ce0d0"`);
        await queryRunner.query(`ALTER TABLE "post" DROP CONSTRAINT "FK_c6fb082a3114f35d0cc27c518e0"`);
        await queryRunner.query(`DROP INDEX "IDX_28bbfc9cd49669ac86e5abee22"`);
        await queryRunner.query(`DROP INDEX "IDX_e27a3cfd22add5dbe01b11e01e"`);
        await queryRunner.query(`DROP TABLE "events_edates_edates"`);
        await queryRunner.query(`DROP INDEX "IDX_fdbedf506060a2c393c4e7daee"`);
        await queryRunner.query(`DROP INDEX "IDX_2662d5ee0a8feea71112e8ce0d"`);
        await queryRunner.query(`DROP TABLE "events_countries_countries"`);
        await queryRunner.query(`DROP TABLE "post"`);
        await queryRunner.query(`DROP TABLE "users"`);
        await queryRunner.query(`DROP TABLE "events"`);
        await queryRunner.query(`DROP TABLE "edates"`);
        await queryRunner.query(`DROP TABLE "countries"`);
    }

}
