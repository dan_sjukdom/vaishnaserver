import { Entity, PrimaryColumn, Column, ManyToMany, JoinTable } from "typeorm";
import { Country } from "./Country";
import { EDate } from "./EDate";
import { ObjectType, Field } from "type-graphql";

@ObjectType()
@Entity("events")
export class Event {
	@Field()
	@PrimaryColumn({ type: "uuid" })
	id: string;

	@Field()
	@Column({ nullable: false, unique: true })
	name: string;

	@Field()
	@Column({ nullable: false, type: "text" })
	description: string;

	@Field(() => Country)
	@ManyToMany(() => Country)
	@JoinTable()
	countries: Country[];

	@Field(() => EDate)
	@ManyToMany(() => EDate)
	@JoinTable()
	edates: EDate[];
}
