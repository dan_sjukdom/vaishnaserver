import { ObjectType, Field, Int } from "type-graphql";
import { Entity, PrimaryColumn, Column, OneToMany } from "typeorm";
import { Post } from "./Post";

// Turn classes into graphql types by decorating the entity
// - Field(() => <Type>)
//  is used to avoid circular dependencies

@ObjectType()
@Entity("users")
export class User {
	@Field()
	@PrimaryColumn({ type: "uuid" })
	readonly id: string;

	@Field()
	@Column({ nullable: false, unique: true })
	username: string;

	@Field()
	@Column({ nullable: false, unique: true })
	email: string;

	@Field()
	@Column({ nullable: true })
	spiritual_name?: string;

	@Field(() => String)
	@Column({ type: "date", default: new Date() })
	createdAt: Date;

	@Column({ type: "varchar", nullable: false })
	password: string;

	@OneToMany(() => Post, (post) => post.author, { cascade: true })
	posts: Post[];
}
