import { Entity, PrimaryColumn, Column } from "typeorm";
import { ObjectType, Field } from "type-graphql";

@ObjectType()
@Entity("countries")
export class Country {
	@Field()
	@PrimaryColumn({ type: "uuid" })
	id: string;

	@Field()
	@Column({ nullable: false, unique: true })
	name: string;

	@Field()
	@Column({ nullable: false, unique: true })
	country_code: string;
}
