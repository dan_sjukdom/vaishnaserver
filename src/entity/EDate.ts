import { Entity, PrimaryColumn, Column } from "typeorm";
import { ObjectType, Field, Int } from "type-graphql";

@ObjectType()
@Entity("edates")
export class EDate {
	@Field()
	@PrimaryColumn({ type: "uuid" })
	id: string;

	@Field()
	@Column({ nullable: false, type: "timestamp with time zone" })
	date: string;
}
