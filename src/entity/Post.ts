import {
	Entity,
	PrimaryColumn,
	Column,
	ManyToOne,
	CreateDateColumn,
	UpdateDateColumn,
} from "typeorm";
import { User } from "./User";
import { Field } from "type-graphql";

@Entity("posts")
export class Post {
	@Field()
	@PrimaryColumn({ type: "uuid" })
	id: string;

	@Field()
	@Column({ nullable: false })
	title: string;

	@Field()
	@Column({ nullable: false, type: "text" })
	content: string;

	@ManyToOne(() => User, (user) => user.posts)
	author: User;

	@Field(() => Date)
	@CreateDateColumn({ name: "created_at" })
	created_at: Date;

	@Field(() => Date)
	@UpdateDateColumn({ name: "updated_at" })
	updated_at: Date;
}
