import { Query, Resolver } from "type-graphql";

@Resolver()
export class HelloResolver {
	// What the query returns
	// the type must be capitalized
	@Query(() => String)
	hello() {
		return "hello world";
	}
}
