import {
	Query,
	Resolver,
	Mutation,
	Arg,
	ObjectType,
	Field,
} from "type-graphql";
import { InjectRepository } from "typeorm-typedi-extensions";
import { Repository } from "typeorm";
import { User } from "../entity/User";
import argon2 from "argon2";
import { v4 as uuidv4 } from "uuid";

@ObjectType()
class FieldError {
	@Field()
	field: string;
	@Field()
	message: string;
}

@ObjectType()
class UserResponse {
	@Field(() => [FieldError], { nullable: true })
	errors?: FieldError[];

	@Field(() => User, { nullable: true })
	user?: User;

	@Field(() => Boolean, { nullable: true })
	success?: boolean;
}

@Resolver(User)
export class UserResolver {
	// We need to return a graphql type

	constructor(
		@InjectRepository(User)
		private readonly userRepository: Repository<User>
	) {}

	@Query(() => [User])
	async users(): Promise<User[]> {
		const users = await this.userRepository.find();
		return users;
	}

	@Query(() => User)
	async findUserById(@Arg("id") id: string): Promise<User> {
		const user = await this.userRepository.findOne({ id: id });
		return user;
	}

	@Mutation(() => UserResponse)
	async createUser(
		@Arg("username") username: string,
		@Arg("email") email: string,
		@Arg("spiritual_name", { nullable: true })
		spiritual_name: string,
		@Arg("password") password: string
	): Promise<UserResponse> {
		try {
			const hashedPassword = await argon2.hash(password, {
				type: argon2.argon2id,
				memoryCost: 2 ** 16,
				hashLength: 50,
			});

			const id = uuidv4();

			const newUser = this.userRepository.create({
				id: id,
				username,
				email,
				spiritual_name,
				password: hashedPassword,
			});

			return {
				user: await this.userRepository.save(newUser),
			};
		} catch (e) {
			console.log("Error creating the user");
			console.log(e);
			if (e.code === "23505") {
				const field = e.detail
					.match(/^Key (.*)=.*$/)[1]
					.replace(/[()]/g, "");
				return {
					errors: [
						{
							field: field,
							message: e.detail,
						},
					],
				};
			}
		}
	}

	@Mutation(() => UserResponse)
	async deleteUserByEmail(
		@Arg("email") email: string
	): Promise<UserResponse> {
		try {
			const result = await this.userRepository.delete({ email: email });
			const { affected } = result;

			return {
				success: affected > 0 ? true : false,
			};
		} catch (e) {
			return {
				errors: [
					{
						field: "",
						message: "",
					},
				],
			};
		}
	}

	@Mutation(() => UserResponse)
	async login(
		@Arg("email") email: string,
		@Arg("password") password: string
	): Promise<UserResponse> {
		try {
			const {
				password: hashedPassword,
			} = await this.userRepository.findOne({ email: email });
			const valid = await argon2.verify(hashedPassword, password);
			if (valid) {
				return {
					success: true,
				};
			} else {
				return {
					errors: [
						{
							field: "password",
							message: "The email or password is invalid",
						},
					],
					success: false,
				};
			}
		} catch (e) {}
	}

	@Mutation(() => User)
	async editUser(
		@Arg("id", () => String) id: number,
		@Arg("email", () => String, { nullable: true }) email: string,
		@Arg("username", () => String, { nullable: true }) username: string,
		@Arg("spiritual_name", () => String, { nullable: true })
		spiritual_name: string
	) {
		try {
			const updateObject = Object.entries({
				username: username,
				email: email,
				spiritual_name: spiritual_name,
			})
				.filter((item) => item[1] !== undefined)
				.reduce((acc, value) => ({ ...acc, [value[0]]: value[1] }), {});

			console.log(updateObject);

			const result = await this.userRepository
				.createQueryBuilder()
				.where({ id: id })
				.update(updateObject)
				.execute();

			return result.affected > 0 ? true : false;
		} catch (e) {}
	}
}
