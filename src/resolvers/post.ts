import { Query, Mutation, Resolver, Arg } from "type-graphql";
import { InjectRepository } from "typeorm-typedi-extensions";
import { Repository } from "typeorm";
import { Post } from "../entity/Post";
import { User } from "../entity/User";

@Resolver()
export class PostResolver {
	constructor(
		@InjectRepository(Post)
		private readonly postRepository: Repository<Post>,
		@InjectRepository(User)
		private readonly userRepository: Repository<User>
	) {}

	@Query(() => [Post])
	async posts() {
		const posts = await this.postRepository.find();
		return posts;
	}

	@Query(() => Post)
	async getPostById(@Arg("id") id: string) {
		const post = await this.postRepository.findOne({ id: id });
		return post;
	}

	@Query(() => Post)
	async getPostByUserEmail(@Arg("email") email: string) {
		const post = await this.postRepository
			.createQueryBuilder("post")
			.innerJoinAndSelect("post.author", "author")
			.where("author.email = :email", { email })
			.getMany();

		return post;
	}

	@Mutation(() => Post)
	async createPost(
		@Arg("title") title: string,
		@Arg("content") content: string
	) {
		const post = this.postRepository.create({
			title: title,
			content: content,
		});

		return post;
	}
}
