import { Arg, Mutation, Query, Resolver } from "type-graphql";
import { Country } from "../entity/Country";
import { InjectRepository } from "typeorm-typedi-extensions";
import { Repository } from "typeorm";
import { runInThisContext } from "vm";

@Resolver()
export class CountryResolver {
	constructor(
		@InjectRepository(Country)
		private readonly countryRepository: Repository<Country>
	) {}

	@Query(() => [Country])
	async countries() {
		const countries = await this.countryRepository.find();
		return countries;
	}

	@Query(() => Country)
	async country(@Arg("country_code", () => String) country_code: string) {
		const country = await this.countryRepository.findOne({
			country_code: country_code,
		});
		return country;
	}
}
