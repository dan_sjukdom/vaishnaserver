import { Resolver, Query, Mutation, Arg } from "type-graphql";
import { InjectRepository } from "typeorm-typedi-extensions";
import { Repository } from "typeorm";
import { Event } from "../entity/Event";
import { Country } from "../entity/Country";
import { EDate } from "../entity/EDate";

@Resolver()
export class EventResolver {
	constructor(
		@InjectRepository(Event)
		private readonly eventRepository: Repository<Event>,
		@InjectRepository(Country)
		private readonly countryRepository: Repository<Country>,
		@InjectRepository(EDate)
		private readonly edateRepository: Repository<EDate>
	) {}

	@Query(() => [Event])
	async events() {
		const events = await this.eventRepository.find({
			relations: ["countries", "edates"],
		});

		console.log("-0------");
		console.log(events);
		console.log(events[0].countries);

		return events;
	}

	@Query(() => Event)
	async event(@Arg("id", () => String) id: string) {
		const event = await this.eventRepository.findOne(id, {
			relations: ["countries", "edates"],
		});
		return event;
	}

	@Mutation(() => Event)
	async createEvent(
		@Arg("name", () => String) name: string,
		@Arg("description", () => String) description: string,
		@Arg("country_code", () => String) country_code: string,
		@Arg("event_date", () => String) event_date: string
	): Promise<Event> {
		// Get the country from the batabase
		const country_row = await this.countryRepository.findOne({
			country_code: country_code,
		});

		// Verify if the date exists, create it if not
		let event_date_row = await this.edateRepository.findOne({
			date: event_date,
		});

		if (!event_date_row) {
			event_date_row = this.edateRepository.create({
				date: event_date,
			});

			await this.edateRepository.save(event_date_row);
		}

		const event = this.eventRepository.create({
			name: name,
			description: description,
			countries: [country_row],
			edates: [event_date_row],
		});

		await this.eventRepository.save(event);

		return event;
	}
}
