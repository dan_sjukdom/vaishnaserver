import config from "config";

//const postgres_uri: string = config.get("POSTGRES.URL");

export default {
	type: "postgres",
	host: "localhost",
	port: 5432,
	username: "vaishnabot",
	password: "vaishnabot",
	database: "vaishnadb",
	synchronize: true,
	logging: false,
	entities: ["src/entity/**/*.ts"],
	migrations: ["src/migration/**/*.ts"],
	subscribers: ["src/subscriber/**/*.ts"],
	cli: {
		entitiesDir: "src/entity",
		migrationsDir: "src/migration",
		subscribersDir: "src/subscriber",
	},
};
